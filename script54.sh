#!/bin/bash


#date
echo "The Date Today Is" > /tmp/serverinfo.info
date >> /tmp/serverinfo.info
echo "" >> /tmp/serverinfo.info

#Last 10 Logins
echo "My Last 10 Logins" >> /tmp/serverinfo.info
last -10 >> /tmp/serverinfo.info
echo "" >> /tmp/serverinfo.info


#Swap Space
echo "My Swap Space" >> /tmp/serverinfo.info
free -h | grep -i swap >> /tmp/serverinfo.info
echo "" >> /tmp/serverinfo.info

#Kernal version
echo "My Kernel Version" >> /tmp/serverinfo.info
hostnamectl | grep Kernel >> /tmp/serverinfo.info
echo "" >> /tmp/serverinfo.info

#IP Address
echo "My IP Address" >> /tmp/serverinfo.info
ip a | grep inet >> /tmp/serverinfo.info
echo "" >> /tmp/serverinfo.info

